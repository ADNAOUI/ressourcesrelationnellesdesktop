//VIEWS
import Preview    from '../views/monCompte/preview/Preview.jsx';
import Gallery    from '../views/monCompte/gallery/Gallery.jsx';
import Favorites  from '../views/monCompte/favorites/Favorites.jsx';
import Resources  from '../views/monCompte/resources/Resources.jsx';


//ICONS
import PreviewIcon    from '../components/atoms/img/monCompte/preview.png';
import GalleryIcon    from '../components/atoms/img/monCompte/gallery.png';
import FavoritesIcon  from '../components/atoms/img/monCompte/favorite.png';
import ResourcesIcon  from '../components/atoms/img/monCompte/resources.png';

const routesNavMyAccount = [
  {
    pathNavMyAccount  : "/aperçu",
    nameNavMyAccount  : "Aperçu",
    iconNavMyAccount  : PreviewIcon,
    component         : Preview,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte/route",
  },
  {
    pathNavMyAccount  : "/galerie",
    nameNavMyAccount  : "Galerie",
    iconNavMyAccount  : GalleryIcon,
    component         : Gallery,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte/route",
  },
  {
    pathNavMyAccount  : "/favoris",
    nameNavMyAccount  : "Favoris",
    iconNavMyAccount  : FavoritesIcon,
    component         : Favorites,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte/route",
  },
  {
    pathNavMyAccount  : "/ressources",
    nameNavMyAccount  : "Ressources",
    iconNavMyAccount  : ResourcesIcon,
    component         : Resources,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte/route",
  }
];
export default routesNavMyAccount;