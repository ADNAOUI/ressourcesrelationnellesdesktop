//----- MODULES -----//
import * as React from 'react';
import axios from 'axios';

//----- COMPOSANTS -----//
import MediaContent from '../../components/molecules/displayRessource/MediaContent';
import InfoContent from '../../components/molecules/displayRessource/InfoContent';
import ResourceIconBar from '../../components/molecules/displayRessource/ResourceIconBar';
import SocialContent from '../../components/molecules/displayRessource/SocialContent';

//----- STYLES -----//
import './DisplayRessource.css';

const DisplayRessource = () => {
  const [ressource, setRessource] = React.useState([]);

  const loadData = () => {
    const URLcourante = document.location.href.split('/');
    const id_ressource = URLcourante[5];

    axios.get(`https://ms-lab-directus.fun/items/rr_ressources/${id_ressource}`).then(res => {
      const list = res.data.data
      setRessource(list);
    });
    window.scrollTo(0, 0);
  }

  React.useEffect(() => {
    loadData()
  }, [])

  return (
    <section>
      <div className="container-fluid display-ressource-all-components">
        <div className="row">
          <div className="col-sm-12 col-lg-9">
            <SocialContent />
            <MediaContent
              title={ressource.TITRE_RESSOURCE}
              image_ressource={ressource.IMAGE_RESSOURCE}
              content={ressource.CONTENU_RESSOURCE}
            />
            <ResourceIconBar
              nbr_heart={ressource.NOMBRE_LIKE}
              nbr_comments={ressource.NOMBRE_COMMENTAIRE}
              nbr_share={ressource.NOMBRE_PARTAGE}
            />
          </div>
          <div className="col-sm-12 col-lg-3 ">
            <InfoContent
              image_membre={ressource.IMAGE_PROFIL_MEMBRE}
              pseudo={ressource.PSEUDO_MEMBRE}
              job={ressource.PROFESSION_MEMBRE}
              category={ressource.DESIGNATION_CATEGORIE_RESSOURCE}
              type_ressource={ressource.DESIGNATION_TYPE_RELATION}
              type_relation={ressource.DESIGNATION_TYPE_RESSOURCE}
              date={ressource.DATE_PUBLICATION_RESSOURCE}
              content={ressource.CONTENU_RESSOURCE}
            />
          </div>
        </div>
      </div>
    </section>
  );
}
export default DisplayRessource;