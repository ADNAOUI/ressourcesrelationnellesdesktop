//MODULES
import React, { Fragment, Component } from 'react';

//COMPONENTS
import EncadrementGallery from '../../../components/molecules/encadrement/encadrementGallery/EncadrementGallery.jsx';

//STYLES
import './gallery.css';

class Gallery extends Component {
    render(){
        return(
            <Fragment>
                <div className="lineMyAccount">
                    <div className="contentGallery">
                        <EncadrementGallery title="Ma galerie" />
                    </div>
                </div>

            </Fragment>
        )
    }
}

export default Gallery;