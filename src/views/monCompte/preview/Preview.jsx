//MODULES
import React, { Fragment, Component } from 'react';

//COMPONENTS
import EncadrementPreview from '../../../components/molecules/encadrement/encadrementPreview/EncadrementPreview.jsx';
import EncadrementPreviewFavoris from '../../../components/molecules/encadrement/encadrementPreviewFavoris/EncadrementPreviewFavoris.jsx';
import EncadrementPostResources from '../../../components/molecules/encadrement/encadrementPostResources/EncadrementPostResources.jsx';

//STYLES
import './preview.css';
class MyAccountPreview extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }
    render(){
        return(
            <Fragment>
                <div className="contentMyAccount">
                    <div className="lineMyAccount">
                        <div className="contentPreview">
                            <EncadrementPreview title="A propos de" />
                        </div>
                        <div className="contentFavoris">
                            <EncadrementPreviewFavoris title="Derniers favoris" />
                        </div>
                    </div>
                    <div className="lineMyAccount">
                        <div className="contentPostResources">
                            <EncadrementPostResources title="Dernières ressources postées" />
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export default MyAccountPreview;