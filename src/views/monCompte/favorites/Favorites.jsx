//MODULES
import React, { Fragment, Component } from 'react';
import Axios from 'axios';

//COMPONENTS
import EncadrementFavorite from '../../../components/molecules/encadrement/encadrementFavorite/EncadrementFavorite.jsx';

//STYLES
import './favorites.css';

class Favorites extends Component {
    constructor(props){
        super(props);
        this.state = {
            ressources : [],
        }
    }
    getResources = () => {
        Axios({
            method:       'get',
            url:          `localhost:3000//items/rr_ressources?limit=4`,
            responseType: 'json'
        }).then(response => {
            this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
        })
    }
    componentDidMount = () => {
        this.getResources();
    }
    render() {
        return (
            <Fragment>
                <div className="contentFavorite">
                    <EncadrementFavorite title="Ressources favorites" />
                </div>
            </Fragment>
        );
    }
}
export default Favorites;