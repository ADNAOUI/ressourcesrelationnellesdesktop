//MODULES
import React, { Fragment, Component } from 'react';
import Axios from 'axios';

//COMPONENTS
import EncadrementResources from '../../../components/molecules/encadrement/encadrementResources/EncadrementResources.jsx';

//STYLES
import './resources.css';

class Resources extends Component {
    constructor(props){
        super(props);
        this.state = {
            ressources : [],
        }
    }
    getResources = () => {
        Axios({
            method:       'get',
            url:          `localhost:3000//items/rr_ressources?limit=4`,
            responseType: 'json'
        }).then(response => {
            this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
        })
    }
    componentDidMount = () => {
        this.getResources();
    }
    render() {
        return (
            <Fragment>
                <div className="contentResources">
                    <EncadrementResources title="Mes Ressources" />
                </div>
            </Fragment>
        );
    }
}
export default Resources;