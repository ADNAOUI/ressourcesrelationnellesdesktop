//*----- MODULES -----//
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

//*----- COMPOSANTS -----//
import ResourceCard from '../../components/organismes/ressourceCard/ResourceCard.jsx';
import Spinner from '../../components/molecules/spinner/spinner'

//CSS
import './Home.css';

const Home = (props) => {
  const url = 'https://ms-lab-directus.fun/items/rr_ressources?limit=20'
  const [ressources, setRessources] = useState([]);
  const [page, setPage] = useState(2);
  const [isFetching, setIsFetching] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  const loadData = async () => {
    await axios.get(url)
      .then(res => {
        const datas = res.data.data
        setRessources(datas.sort(() => Math.random() - 0.5));
        setIsLoading(false);
      });
    window.scrollTo(0, 0);
  }

  const moreData = async () => {
    var datasRessources = url + `&page=${page}`;
    await axios.get(datasRessources).then(res => {
      setRessources([...ressources, ...res.data.data]);
      setPage(page + 1)
      setIsFetching(false)
    });
  }

  const isScrolling = () => {
    let bottomPage = window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight;
    if (bottomPage) {
      return;
    }
    setIsFetching(true)
  }

  useEffect(() => {
    loadData()
    window.addEventListener("scroll", isScrolling);
    return () => window.removeEventListener("scroll", isScrolling);
  }, [])

  useEffect(() => {
    if (isFetching) {
      document.getElementById("parentLoadingData").classList.add("isFetching")
      moreData();
    }
  }, [isFetching]);

  return (
    <>
      <section>
        {isLoading ? <Spinner classSpinner="loading-spinner-relative" /> :
          <div className="parent-display-ressources">
            <ul className="display-ressources-list">
              {ressources.map((ressource, i) =>
                <Link key={i} to={`/ressources_relationnelles/displayRessource/${ressource.ID_RESSOURCE}`}>
                  <ResourceCard
                    date={ressource.DATE_PUBLICATION_RESSOURCE}
                    title={ressource.TITRE_RESSOURCE}
                    image_ressource={ressource.IMAGE_RESSOURCE}
                    category={ressource.DESIGNATION_CATEGORIE_RESSOURCE}
                    type_ressource={ressource.DESIGNATION_TYPE_RESSOURCE}
                    type_relation={ressource.DESIGNATION_TYPE_RELATION}
                    image_membre={ressource.IMAGE_PROFIL_MEMBRE}
                    pseudo={ressource.PSEUDO_MEMBRE}
                    job={ressource.PROFESSION_MEMBRE}
                    nb_like={ressource.NOMBRE_LIKE}
                    nb_comments={ressource.NOMBRE_COMMENTAIRE}
                    nb_share={ressource.NOMBRE_PARTAGE}
                  />
                </Link>
              )}
            </ul>
            {isFetching ? <Spinner classSpinner="loading-spinner-absolute" /> : ""}
          </div>
        }
      </section>
    </>
  );
}


export default Home;