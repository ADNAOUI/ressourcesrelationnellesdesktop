import React, {Component} from 'react';

//CSS
import './Conditions.css';

export default class Conditions extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render(){
    return(
      <section>
        <h1 className="titleVisitorSitePresentation">Les conditions à respecter au sein de la communauté</h1>
            <div className="col-12 parent-position-element-conditions">
                <ol className="col-10">
                  <div>
                    <li>Introduction : Votre accord aux présentes Conditions d'utilisation</li>
                    <p>
                      VEUILLEZ LIRE ATTENTIVEMENT LES PRÉSENTES CONDITIONS D'UTILISATION. CE CONTRAT VOUS ENGAGE. Bienvenue dans les services gérés par Ressources Relationnelles Interactive, Inc. (appelé avec ses affiliés « Ressources Relationnelles » ou « nous »), qui comprennent le site internet disponible à l'adresse https://www.Ressources Relationnelles.tv et son réseau de sites internet, de logiciels et de tout autre produit ou service offert par Ressources Relationnelles (les « services Ressources Relationnelles »). Les autres services proposés par Ressources Relationnelles peuvent faire l'objet de conditions distinctes.
                      En utilisant les services Ressources Relationnelles, vous serez soumis aux Lignes de conduite de la communauté de Ressources Relationnelles et à d'autres lignes de conduite et règles publiées sur les services Ressources Relationnelles qui sont mises à votre disposition ou vous sont communiquées en rapport avec des services et fonctionnalités spécifiques. Ressources Relationnelles peut également offrir certains services payants, qui sont soumis aux Conditions de vente de Ressources Relationnelles ainsi qu'aux conditions supplémentaires qui vous sont divulguées en lien avec ces services. L'ensemble de ces conditions et lignes de conduite (les «lignes de conduite») sont incorporées dans les présentes Conditions d'utilisation par référence.
                      Les Conditions d'utilisation s'appliquent que vous soyez un utilisateur qui crée un compte auprès des services Ressources Relationnelles ou un utilisateur sans compte. Vous acceptez qu'en cliquant sur « S'inscrire » ou en vous inscrivant, en téléchargeant, en utilisant les services Ressources Relationnelles ou en y ayant accès, vous concluez un accord juridique entre vous et Ressources Relationnelles concernant votre utilisation des services Ressources Relationnelles. Vous reconnaissez que vous avez lu, compris et accepté d'être lié par les présentes Conditions d'utilisation. Si vous n'acceptez pas ces Conditions d'utilisation, n'accédez à aucun des services Ressources Relationnelles et ne les utilisez pas.
                      Lorsque vous utilisez ou ouvrez un compte Ressources Relationnelles au nom d'une société, d'une entité ou d'un organisme (collectivement « organisme souscripteur »), vous déclarez et garantissez que vous : (i) êtes un représentant autorisé de l'organisme souscripteur ayant le pouvoir de lier cet organisme aux présentes Conditions d'utilisation et d'accorder les autorisations énoncées ; et (ii) acceptez les présentes Conditions d'utilisation au nom de l'organisme souscripteur.
                    </p>
                  </div>

                  <div>
                    <li>Utilisation de Ressources Relationnelles par les mineurs et les personnes bloquées</li>
                    <p>
                      Les services Ressources Relationnelles ne sont pas accessibles aux personnes de moins de 13 ans. Si vous avez entre 13 ans et l'âge de la majorité légale dans votre pays de résidence, vous ne pouvez utiliser les Services Ressources Relationnelles que sous la supervision d'un parent ou d'un tuteur légal qui accepte d'être lié par les présentes Conditions d'utilisation.
                      Les services Ressources Relationnelles ne sont pas non plus accessibles aux utilisateurs déjà exclus des services Ressources Relationnelles par Ressources Relationnelles, ou à toute personne à laquelle il est interdit de les recevoir en vertu des lois des États-Unis (telles que ses restrictions et règlements sur l'exportation et la réexportation) ou de toute autre loi applicable dans toute autre juridiction.
                      EN TÉLÉCHARGEANT, EN INSTALLANT OU EN UTILISANT LES SERVICES Ressources Relationnelles, VOUS DÉCLAREZ AVOIR AU MOINS 13 ANS, OU, SI VOUS AVEZ ENTRE 13 ANS ET L'ÂGE DE LA MAJORITÉ LÉGALE DANS VOTRE JURIDICTION DE RÉSIDENCE, QUE VOTRE PARENT OU TUTEUR LÉGAL ACCEPTE D'ÊTRE LIÉ PAR LES PRÉSENTES CONDITIONS D'UTILISATION, ET NE PAS AVOIR DÉJÀ ÉTÉ EXCLU DES SERVICES Ressources Relationnelles ET QUE LEUR RÉCEPTION NE VOUS A PAS ÉTÉ INTERDITE.
                    </p>
                  </div>

                  <div>
                    <li>Avis de confidentialité</li>
                    <p>
                      Ressources Relationnelles considère que votre vie privée est importante. Veuillez consulter notre Avis de confidentialité pour obtenir des informations sur la manière dont nous recueillons, utilisons et divulguons vos informations personnelles et nos Choix de confidentialité pour obtenir des informations sur la façon dont vous pouvez gérer votre vie privée en ligne lorsque vous utilisez les Services Ressources Relationnelles. 
                    </p>
                  </div>

                  <div>
                    <li>Compte</li>
                      <ul className="lower-latin-style">
                        <li>Compte et mot de passe</li>
                        <p>
                          Afin d'ouvrir un compte, il vous sera demandé de nous fournir certaines informations telles qu'un nom de compte et un mot de passe.
                          Vous êtes seul responsable du maintien de la confidentialité de votre compte et mot de passe, et de la restriction de l'accès à votre ordinateur. Si vous autorisez d'autres personnes à utiliser les informations de connexion de votre compte, vous acceptez les présentes Conditions d'utilisation au nom de toutes les autres personnes qui utilisent les services avec votre compte ou votre mot de passe, et vous êtes responsable de toutes les activités qui se produisent avec votre compte ou votre mot de passe. Veuillez vous assurer que les informations que vous fournissez à Ressources Relationnelles lors de votre inscription et à tout autre moment sont vraies, exactes, à jour et complètes à votre connaissance.
                          Sauf autorisation écrite expresse de Ressources Relationnelles, vous ne pouvez pas vendre, louer, partager ou donner accès à votre compte à qui que ce soit d'autre, y compris, sans s'y limiter, facturer à qui que ce soit l'accès aux droits d'administrateur de votre compte. Ressources Relationnelles se réserve tous les droits et recours légaux disponibles pour empêcher l'utilisation non autorisée des services Ressources Relationnelles, y compris, mais sans s'y limiter, les obstacles technologiques, le mappage IP et, dans les cas graves, le fait de contacter directement votre fournisseur d'accès internet (FAI) concernant une telle utilisation non autorisée.
                        </p>
                        <li>Comptes tiers</li>
                        <p>
                          Ressources Relationnelles peut vous permettre de vous inscrire et de vous connecter aux services Ressources Relationnelles par le biais de certains services tiers. La collecte, l'utilisation et la divulgation de vos informations par un tiers seront soumises à la politique de confidentialité de ce service tiers. Vous trouverez de plus amples informations sur la façon dont Ressources Relationnelles recueille, utilise et divulgue vos informations personnelles lorsque vous liez votre compte Ressources Relationnelles à votre compte sur un service tiers dans notre Avis de confidentialité.
                        </p>
                      </ul>
                  </div>

                  <div>
                    <li>Utilisation des appareils et des services</li>
                    <p>
                      L'accès aux Services Ressources Relationnelles peut nécessiter l'utilisation de votre ordinateur personnel ou de votre appareil mobile, ainsi que les communications ou l'utilisation de l'espace sur ces appareils. Vous êtes responsable de tous les frais de connexion Internet ou de téléphone mobile que vous encourez lorsque vous accédez aux Services Ressources Relationnelles.
                    </p>
                    <li>Modification des présentes Conditions d'utilisation</li>
                    <p>
                      Ressources Relationnelles peut modifier des clauses des présentes Conditions d'utilisation en publiant les conditions modifiées. Si vous continuez d'utiliser les services Ressources Relationnelles après la date d'entrée en vigueur des Conditions d'utilisation modifiées, vous acceptez les conditions.
                      Ressources Relationnelles fournira aux résidents de la République de Corée un préavis raisonnable pour toute modification matérielle de ses Conditions d'utilisation.  Toutes les modifications entrent en vigueur au plus tôt 30 jours civils après leur publication ; toutefois, toute modification concernant de nouvelles fonctionnalités du service, des fonctionnalités du service qui sont bénéfiques pour l'utilisateur ou des modifications apportées pour des raisons juridiques peut entrer en vigueur immédiatement.
                    </p>
                    <li>Licence</li>
                    <p>
                      Les services Ressources Relationnelles sont la propriété de Ressources Relationnelles et sont exploités par Ressources Relationnelles. Sauf indication contraire, tous les contenus, informations et autres éléments sur les services Ressources Relationnelles (à l'exclusion du contenu utilisateur, tel que défini à l'article 8 ci-après), y compris, sans s'y limiter, les marques et logos de Ressources Relationnelles, les interfaces visuelles, le graphisme, la conception, la compilation, les informations, les logiciels, le code informatique (y compris le code source ou le code objet), les services, les textes, les images, les données, les fichiers audio, les autres fichiers et la sélection et la disposition de ceux-ci (collectivement, les « éléments ») sont protégés par les droits de propriété intellectuelle et les lois ainsi que les droits de propriété intellectuelle pertinents. Tous les éléments sont la propriété de Ressources Relationnelles, de ses filiales ou sociétés affiliées, ou de tiers concédants de licence. Sauf indication contraire expresse et écrite de Ressources Relationnelles, en acceptant les présentes Conditions d'utilisation, vous bénéficiez d'une licence limitée et non cessible (c'est-à-dire un droit personnel et limité) pour accéder aux services Ressources Relationnelles et les utiliser pour votre usage personnel ou professionnel interne uniquement.
                      Ressources Relationnelles se réserve tous les droits qui ne sont pas expressément accordés dans les présentes Conditions d'utilisation. Cette licence est soumise aux présentes Conditions d'utilisation et ne vous permet pas de vous engager dans l'une ou l'autre des activités suivantes : (a) la revente ou l'utilisation commerciale des services Ressources Relationnelles ou des éléments ; (b) la distribution, la représentation publique ou l'affichage public des éléments ; (c) la modification ou toute autre utilisation dérivée des services Ressources Relationnelles ou des éléments, ou toute partie de ceux-ci ; (d) l'utilisation de toute méthode d'extraction de données, robots ou méthodes similaires de collecte ou d'extraction de données ; (e) le téléchargement (à l'exception de la mise en cache des pages) de toute partie des services Ressources Relationnelles, des éléments ou de toute information qu'ils contiennent, sauf dans les cas expressément autorisés sur les services Ressources Relationnelles ; ou (f) toute utilisation des services Ressources Relationnelles ou des éléments, sauf aux fins prévues. Toute utilisation des services Ressources Relationnelles ou des éléments, à l'exception de ce qui est expressément autorisé dans les présentes Conditions d'utilisation, sans l'autorisation écrite préalable de Ressources Relationnelles, est strictement interdite et peut constituer une atteinte aux droits de propriété intellectuelle ou à d'autres lois. Sauf mention expresse dans les présentes Conditions d'utilisation, rien dans celles-ci ne doit être interprété comme conférant une licence de droits de propriété intellectuelle, que ce soit par préclusion, implication ou autres principes juridiques. Ressources Relationnelles peut résilier cette licence, comme décrit dans la section 14.
                    </p>
                  </div>

                  <div>
                    <li>Contenu Utilisateur</li>
                    <p>
                      Ressources Relationnelles vous permet de diffuser en continu des œuvres audiovisuelles en direct et préenregistrées, d'utiliser des services tels que le chat, les forums, les contributions sur wiki, les services interactifs vocaux, et de participer à d'autres activités dans lesquelles vous pouvez créer, publier, transmettre, exécuter ou stocker du contenu, des messages, du texte, du son, des images, des applications, des codes ou d'autres données ou documents sur les services Ressources Relationnelles (« contenu utilisateur »).
                    </p>
                      <ul className="lower-latin-style">
                        <li>Licence de Ressources Relationnelles</li>
                        <p>
                          (i) Sauf disposition contraire dans un accord écrit entre vous et Ressources Relationnelles signé par un représentant autorisé de Ressources Relationnelles, si vous soumettez, transmettez, affichez, exécutez, publiez ou stockez du contenu utilisateur en utilisant les services Ressources Relationnelles, vous accordez à Ressources Relationnelles et à ses sous-licenciés, dans la mesure et pour la durée maximale prévue par la loi applicable (y compris à perpétuité si la loi applicable le permet) un droit illimité, mondial, irrévocable, pouvant donner lieu à l'octroi d'une sous-licence, non exclusif, et libre de redevance de (a) utiliser, reproduire, modifier, adapter, publier, traduire, créer des œuvres dérivées, distribuer, exécuter et afficher ce contenu utilisateur (y compris, sans s'y limiter, pour promouvoir et redistribuer tout ou partie des services Ressources Relationnelles (et les œuvres dérivées de ceux-ci) sous toute forme, format, support ou canaux médiatiques connus ou développés ou découverts ultérieurement ; et (b) utiliser le nom, l'identité, la ressemblance et la voix (ou d'autres informations biographiques) que vous soumettez en lien avec ce contenu utilisateur. Si ce contenu utilisateur contient le nom, l'identité, la ressemblance et la voix (ou d'autres informations biographiques) de tiers, vous déclarez et garantissez que vous avez obtenu les consentements et/ou licences approprié(e)s pour utiliser ces caractéristiques et que Ressources Relationnelles et ses sous-licenciés sont autorisés à les utiliser dans la mesure indiquée dans les présentes Conditions d'utilisation.
                        <br/>
                          (ii) En ce qui concerne le contenu utilisateur appelé « add-ons », « maps », « mods », ou d'autres types de projets soumis par le biais de CurseForge.com ou de sites connexes (« projets soumis »), les droits que vous accordez en vertu des présentes prennent fin une fois que vous supprimez ou effacez ces projets soumis des services Ressources Relationnelles. Vous reconnaissez également que Ressources Relationnelles peut conserver, mais sans les afficher, les diffuser ou les exécuter, des copies sur serveur des projets soumis qui ont été supprimés ou effacés.
                        <br/>
                          (iii) En ce qui concerne la diffusion en continu d'œuvres audiovisuelles en direct et préenregistrées, les droits que vous accordez en vertu des présentes prennent fin une fois que vous supprimez ce contenu utilisateur des services Ressources Relationnelles, ou généralement en clôturant votre compte, sauf : (a) dans la mesure où vous l'avez partagé avec d'autres dans le cadre des services Ressources Relationnelles et d'autres parties copiées ou stockées du contenu utilisateur (par exemple, pour en faire un extrait) ; (b) si Ressources Relationnelles l'a utilisé à des fins promotionnelles ; et (c) pour la durée raisonnable nécessaire à la suppression de la sauvegarde et d'autres systèmes.
                        </p>
                        <li>Déclarations et garanties relatives au Contenu Utilisateur</li>
                        <p>
                          Vous êtes seul responsable de votre contenu utilisateur et des conséquences de son affichage ou de sa publication. Vous déclarez et garantissez que : (1) vous êtes le créateur du contenu utilisateur, ou vous détenez ou contrôlez les droits et l'autorité suffisants pour accorder les droits accordés aux présentes ; (2) votre contenu utilisateur (a) n'enfreint pas, ne porte atteinte ou ne détourne pas de manière inappropriée et n'enfreindra pas, ne portera pas atteinte ou ne détournera pas de manière inappropriée les droits de tiers, y compris tout droit d'auteur, marque de commerce, brevet, secret commercial, droit moral, droit à la vie privée, droit à l'image ou tout autre droit de propriété intellectuelle ou de propriété, ou (b) ne diffame pas et ne diffamera pas une autre personne ; (3) votre contenu utilisateur ne contient aucun virus, logiciel publicitaire, logiciel espion, vers, ou tout autre code nuisible ou malveillant ; et (4) à moins que vous n'ayez reçu une autorisation écrite préalable, votre contenu utilisateur ne contient spécifiquement aucune version bêta de logiciel ou contenu de jeu avant la sortie ou non public, ni aucune information confidentielle de Ressources Relationnelles ou de tiers. Ressources Relationnelles se réserve tous les droits et recours contre tout utilisateur qui enfreint ces déclarations et garanties.
                        </p>
                        <li>Le contenu est téléchargé à vos risques et périls.</li>
                        <p>
                          Ressources Relationnelles utilise des mesures de sécurité raisonnables afin de tenter de protéger le contenu utilisateur contre la copie et la distribution non autorisées. Toutefois, Ressources Relationnelles ne garantit pas l'absence de copie, d'utilisation ou de distribution non autorisée du contenu utilisateur par des tiers. Dans la mesure permise par la loi applicable, vous acceptez que Ressources Relationnelles ne soit pas tenu responsable de toute copie, utilisation ou distribution non autorisée du contenu utilisateur par des tiers et vous renoncez à jamais à toute réclamation que vous pourriez avoir contre Ressources Relationnelles pour toute copie ou utilisation non autorisée du contenu utilisateur, quelle qu'en soit la théorie. LES MESURES DE SÉCURITÉ VISANT À PROTÉGER LE CONTENU UTILISATEUR UTILISÉ PAR Ressources Relationnelles SONT FOURNIES ET UTILISÉES « EN L'ÉTAT » ET SANS GARANTIE, CONDITION, ASSURANCE OU DISPOSITION INDIQUANT QUE CES MESURES DE SÉCURITÉ RÉSISTERONT AUX TENTATIVES D'ÉLUDER LES MÉCANISMES DE SÉCURITÉ OU QU'IL N'Y AURA PAS DE BRÈCHES, D'INCAPACITÉS OU D'AUTRES CONTOURNEMENTS DE CES MESURES DE SÉCURITÉ.
                        </p>
                        <li>Promotions</li>
                        <p>
                          Les utilisateurs peuvent promouvoir, administrer ou mener une promotion (un concours ou un tirage au sort) sur les services Ressources Relationnelles, par leur biais ou en les utilisant (une « promotion »). Si vous choisissez de promouvoir, d'administrer ou de mener une promotion, vous devez respecter les règles suivantes : (1) Vous pouvez mettre en œuvre des promotions dans la mesure permise par la loi applicable et vous êtes seul responsable de vous assurer que toutes les promotions sont conformes à toutes les lois, obligations et restrictions en vigueur. (2) Vous serez considéré comme le promoteur de votre promotion dans la ou les juridictions applicables et vous serez seul responsable de tous les aspects et dépenses liés à votre promotion, y compris, sans s'y limiter, l'exécution, l'administration et le fonctionnement de la promotion ; la rédaction et la publication de tout règlement officiel ; la sélection des gagnants ; la remise des prix ; et l'obtention de toutes les autorisations et approbations tierces nécessaires, y compris, sans s'y limiter, le dépôt des inscriptions et cautionnements nécessaires. Ressources Relationnelles a le droit de retirer votre Promotion des services Ressources Relationnelles si Ressources Relationnelles pense raisonnablement que votre Promotion ne respecte pas les Conditions d'utilisation ou la loi applicable. (3) Ressources Relationnelles n'est pas responsable de ces promotions et ne les promeut ni ne les soutient. Vous ne pouvez pas indiquer que Ressources Relationnelles parraine ou coparraine la Promotion. (4) Vous afficherez ou lirez l'annonce qui suit lors de la promotion, de l'administration ou de la conduite d'une promotion : « Ceci est une promotion de [votre nom]. Ressources Relationnelles ne parraine pas et ne soutient pas cette promotion, et n'en est pas responsable ».
                        </p>
                        <li>Soutiens/Témoignages</li>
                        <p>
                          Vous acceptez que votre Contenu Utilisateur sera conforme aux Directives de la FTC concernant l'utilisation des témoignages et des promotions dans la publicité, au Guide de divulgation de la FTC, aux Directives de la FTC en matière de publicité locale, ainsi qu'à toute autre directive émise par la FTC de temps à autre (« Directives de la FTC »), ainsi que toute autre directive en matière de publicité requise par la loi applicable. Par exemple, si vous avez reçu une rémunération ou des produits gratuits en échange de la présentation ou de la promotion d'un produit ou d'un service par le biais des services Ressources Relationnelles, ou si vous êtes un employé d'une entreprise et que vous décidez de présenter ou de promouvoir les produits ou services de cette entreprise par le biais des services Ressources Relationnelles, vous acceptez de vous conformer aux exigences des directives de la FTC relatives à la divulgation de ces relations. Vous, et non Ressources Relationnelles, êtes seul(e) responsable de tout soutien ou témoignage que vous faites concernant un produit ou service par le biais des services Ressources Relationnelles.
                        </p>
                        <li>Activités politiques</li>
                        <p>
                          Dans le respect de ces Conditions d'utilisation et des Lignes de conduite de la communauté, vous pouvez partager vos opinions politiques ; participer à des activités politiques ; fournir des liens vers le site internet officiel d'un comité politique, y compris la page de contribution d'un comité politique ; et demander aux spectateurs de faire des dons directement à un comité politique. Vous acceptez cependant que ces activités politiques n'appartiennent qu'à vous. De plus, en pratiquant ces activités, vous déclarez et garantissez que vous avez le droit de le faire en vertu des lois applicables et que vous respecterez toutes les lois et régulations pertinentes en le faisant.
                          Vous acceptez de ne pas utiliser ou solliciter à utiliser les outils de monétisation de Ressources Relationnelles (par exemple, les bits et abonnements) pour faire ou transmettre des dons à un candidat, au comité d'un candidat, à un comité d'action politique, à un comité de vote ou à tout autre comité de campagne, ou dans le but d'influencer une élection. Les candidats à un poste politique n'ont pas le droit d'utiliser les outils de monétisation de Ressources Relationnelles sur leur chaîne.
                        </p>
                      </ul>
                  </div>

                  <div>
                    <li>Comportement interdit</li>
                    <p>
                      VOUS VOUS ENGAGEZ à ne porter atteinte à aucune loi, contrat, propriété intellectuelle ou autre droit d'un tiers et à ne commettre aucun délit, et vous êtes seul responsable de votre comportement sur les services Ressources Relationnelles.
                      Vous acceptez de vous conformer aux présentes Conditions d'utilisation et aux Lignes de conduite de la communauté Ressources Relationnelles et de ne pas :
                    </p>
                      <ul className="lower-roman-style">
                        <li>
                          créer, télécharger, transmettre, diffuser ou stocker tout contenu inexact, illégal, contrefait, diffamatoire, obscène, pornographique, portant atteinte à la vie privée ou aux droits à la publicité, harcelant, menaçant, abusif, incendiaire ou autrement répréhensible ;
                        </li>
                        <li>
                          usurper l'identité de toute personne ou entité, prétendre faussement à une affiliation avec toute personne ou entité, ou accéder aux comptes d'autrui sur les services Ressources Relationnelles sans autorisation, falsifier la signature numérique d'une autre personne, dénaturer la source, l'identité ou le contenu des informations transmises via les services Ressources Relationnelles, ou exercer toute autre activité frauduleuse similaire ;
                        </li>
                        <li>
                            envoyer du courrier indésirable aux utilisateurs des services Ressources Relationnelles, y compris, sans s'y limiter, de la publicité non sollicitée, des éléments promotionnels ou tout autre élément de sollicitation, de l'envoi en masse de publicité commerciale, des chaînes d'e-mails, des annonces informationnelles, des demandes de bienfaisance, des pétitions pour signatures ou tout ce qui précède concernant des cadeaux promotionnels (comme les tirages et les concours), et d'autres activités similaires ;
                        </li>
                        <li>
                          collecter ou recueillir les adresses e-mail ou autres informations de contact d'autres utilisateurs des services Ressources Relationnelles ;
                        </li>
                        <li>
                          diffamer, harceler, maltraiter, menacer ou escroquer les utilisateurs des services Ressources Relationnelles, ou recueillir ou tenter de recueillir des informations personnelles sur les utilisateurs ou des tiers sans leur consentement ;
                        </li>
                        <li>
                            supprimer, contourner, désactiver, endommager ou interférer de toute autre manière avec (a) les caractéristiques de sécurité des services Ressources Relationnelles ou du contenu utilisateur, (b) les fonctionnalités qui empêchent ou restreignent l'utilisation ou la copie de tout contenu accessible par le biais des services Ressources Relationnelles, (c) les fonctionnalités qui imposent des limitations sur l'utilisation des services Ressources Relationnelles ou du contenu utilisateur, ou (d) supprimer les notices de droits d'auteur ou autres droits de propriété sur les services Ressources Relationnelles ou le contenu utilisateur ;
                        </li>
                        <li>
                          faire de l'ingénierie inverse, décompiler, désassembler ou tenter de découvrir le code source des services Ressources Relationnelles ou toute partie de ceux-ci, sauf si et uniquement dans la mesure où cette activité est expressément autorisée par la loi de votre juridiction de résidence ;
                        </li>
                        <li>
                          modifier, adapter, traduire ou créer des œuvres dérivées sur la base des services Ressources Relationnelles ou toute partie des services Ressources Relationnelles, sauf si et seulement si une telle activité est expressément autorisée par la loi en vigueur nonobstant cette limitation ;
                        </li>
                        <li>
                          interférer avec ou endommager le fonctionnement des services Ressources Relationnelles ou la jouissance qu'en tire tout utilisateur, par quelque moyen que ce soit, y compris le téléchargement ou la diffusion de virus, logiciels publicitaires, logiciels espion, vers ou autre code malveillant ;
                        </li>
                        <li>
                          relayer des courriels depuis des serveurs de messagerie tiers sans l'autorisation de ce tiers ;
                        </li>
                        <li>
                          accéder à tout site internet, serveur, application logicielle ou autre ressource informatique appartenant à Ressources Relationnelles, utilisée et/ou autorisée par Ressources Relationnelles, y compris, mais sans s'y limiter, les services Ressources Relationnelles, au moyen d'un robot, « spider », « scraper », « crawler » ou autre moyen automatisé à toute fin, ou contourner toute mesure que Ressources Relationnelles peut utiliser pour empêcher ou restreindre l'accès à tout site internet, serveur, application logicielle ou autre ressource informatique appartenant à Ressources Relationnelles, utilisée et/ou autorisée par Ressources Relationnelles, y compris, mais sans s'y limiter, les services Ressources Relationnelles ;
                        </li>
                        <li>
                          manipuler les identifiants afin de dissimuler l'origine de tout Contenu Utilisateur transmis via les Services Ressources Relationnelles ;
                        </li>
                        <li>
                          interférer avec ou perturber les services Ressources Relationnelles ou les serveurs ou réseaux connectés aux services Ressources Relationnelles, ou désobéir à toute exigence, procédure, politique ou réglementation des réseaux connectés aux services Ressources Relationnelles ; utiliser les services Ressources Relationnelles d'une manière qui pourrait interférer avec, perturber, affecter négativement ou empêcher d'autres utilisateurs de profiter pleinement des services Ressources Relationnelles, ou qui pourrait endommager, désactiver, surcharger ou altérer le fonctionnement des services Ressources Relationnelles de quelque manière que ce soit ;
                        </li>
                        <li>
                          utiliser ou tenter d'utiliser le compte d'un autre utilisateur sans l'autorisation de cet utilisateur et de Ressources Relationnelles ;
                        </li>
                        <li>
                          tenter de contourner les techniques de filtrage de contenu que nous employons, ou tenter d'accéder à tout service ou zone des Services Ressources Relationnelles auxquels vous n'êtes pas autorisé à accéder ;
                        </li>
                        <li>
                          tenter d'indiquer de quelque manière que ce soit, sans notre autorisation écrite, que vous avez une relation avec nous ou que nous vous avons soutenu ou fait la promotion de tout produit ou service à quelque fin que ce soit ; et
                        </li>
                        <li>
                          utiliser les services Ressources Relationnelles à des fins illégales ou en violation de toute loi ou réglementation locale, étatique, nationale ou internationale, y compris, sans s'y limiter, les lois régissant les droits de propriété intellectuelle et autres droits de propriété, la protection des données et la confidentialité.
                        </li>
                      </ul>
                    <br/>
                    <p>
                      Dans les limites permises par les lois applicables, Ressources Relationnelles n'assume aucune responsabilité pour tout contenu utilisateur ou pour toute perte ou dommage en résultant, et Ressources Relationnelles n'est pas non plus responsable pour toute erreur, diffamation, calomnie, allégation, omission, mensonge, obscénité, pornographie ou blasphème que vous pourriez rencontrer dans l'utilisation des services Ressources Relationnelles. Vous utilisez les services Ressources Relationnelles à vos risques et périls. En outre, les présentes règles ne créent aucun droit au bénéfice d'un tiers ou toute attente raisonnable que les services Ressources Relationnelles ne contiendront aucun contenu interdit par ces règles.
                      Ressources Relationnelles n'est pas responsable des déclarations ou représentations incluses dans le contenu utilisateur. Ressources Relationnelles n'approuve pas le contenu utilisateur, les opinions, les recommandations ou les conseils qui y sont exprimés, et Ressources Relationnelles décline expressément toute responsabilité en rapport avec le contenu utilisateur. Bien que Ressources Relationnelles n'ait aucune obligation de filtrer, éditer ou surveiller le contenu utilisateur, Ressources Relationnelles se réserve le droit, à sa discrétion absolue, de supprimer, filtrer ou éditer tout contenu utilisateur affiché ou stocké sur les services Ressources Relationnelles à tout moment et pour quelque motif que ce soit sans préavis, et vous êtes seul responsable de créer des copies de sauvegarde et de remplacer tout contenu utilisateur que vous publiez ou stockez sur les services Ressources Relationnelles à vos frais et dépens exclusifs. Toute utilisation des services Ressources Relationnelles en violation de ce qui précède porte atteinte aux présentes Conditions d'utilisation et peut entraîner, entre autres, la résiliation ou la suspension de vos droits d'utilisation des services Ressources Relationnelles.
                      Pour les résidents de la République de Corée, excepté dans le cas où Ressources Relationnelles considère raisonnablement que vous prévenir est légalement interdit (par exemple, lorsque la notification (i) enfreindrait les lois, règlements ou ordonnances applicables des autorités réglementaires ou (ii) compromettrait une enquête en cours menée par une autorité réglementaire) ou que toute notification peut causer un préjudice à vous, à des tiers, à Ressources Relationnelles ou à ses affiliées (par exemple, lorsque la notification nuit à la sécurité des services Ressources Relationnelles), Ressources Relationnelles vous informera sans délai de la raison pour laquelle la mesure appropriée a été prise.
                    </p>
                  </div>

                  <div>
                    <li>Respect du droit d'auteur</li>
                    <p>
                      Ressources Relationnelles respecte la propriété intellectuelle d'autrui et se conforme aux exigences du Digital Millennium Copyright Act (« DMCA » et des autres lois applicables. Si vous êtes propriétaire d'un droit d'auteur ou un agent de celui-ci et que vous pensez que le contenu affiché sur les services Ressources Relationnelles porte atteinte à votre droit d'auteur, veuillez nous en aviser en suivant nos Directives du DMCA, qui comprennent des informations supplémentaires sur nos politiques, ce que vous devez mentionner dans votre avis, et où le soumettre.
                    </p>
                  </div>

                </ol>
           </div>
      </section>
    );
  }
}