import React, {Component} from 'react';

//CSS

export default class PoliticalCookies extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render(){
    return(
      <section>
          <h1 className="titleVisitorSitePresentation">Avis sur la Politique de Cookies</h1>

          <div className="col-12 parent-position-element-conditions">
                <ol className="col-10">
                  <div>
                    <li>Qu'est-ce qu'un cookie ?</li>
                    <p>
                      Les cookies sont de petits fichiers texte qui sont stockés sur votre appareil. Ils sont largement utilisés pour faire fonctionner les sites internet et les applications, ou pour en améliorer le fonctionnement ou l'efficacité. Cela est possible parce que les sites internet et les applications peuvent lire et écrire ces fichiers, ce qui leur permet de reconnaître un appareil spécifique et de garder en mémoire des informations importantes qui rendront votre utilisation d'un site ou d'une application plus pratique (comme en mémorisant vos préférences d'utilisateur).
                    </p>
                  </div>
                  <div>
                    <li>Quels cookies utilisons-nous, et comment les utilisons-nous ?</li>
                    <p>
                      Nous énumérons ci-dessous une liste des différents types de cookies que nous pouvons utiliser sur les Services Ressources Relationnelles.
                      Nous utilisons des cookies de session (qui existe jusqu'à ce que vous fermiez votre navigateur) et des cookies permanents (qui existent jusqu'à ce que vous les supprimiez depuis votre navigateur, ou jusqu'à ce qu'ils expirent conformément à leur durée de vie). Les cookies permanents créés par Ressources Relationnelles sur les services Ressources Relationnelles ont une durée de vie maximale de treize mois, à partir de votre dernière utilisation en date des services Ressources Relationnelles. Les cookies utilisés pour garder en mémoire vos paramètres de confidentialité (comme les préférences publicitaires) peuvent rester dans votre navigateur pour une durée maximale de cinq ans.
                      Nous utilisons des cookies de première et de tierce partie. Les cookies de première partie sont créés et accédés par le domaine d'hôte (le domaine que l'utilisateur consulte, en l'occurrence Ressources Relationnelles). Les cookies de tierce partie sont créés et accédés par des domaines autres que celui que l'utilisateur visite à ce moment.
                      Cookies opérationnels : ces cookies sont essentiels pour le fonctionnement des services Ressources Relationnelles. Ces cookies sont par exemple utilisés pour garder votre compte connecté et mémoriser vos paramètres. Ces cookies ne peuvent pas être désactivés sans entraver gravement votre utilisation des services Ressources Relationnelles.
                    </p>
                  </div>
                  <div>
                    <li>Comment est-ce que Ressources Relationnelles utilise les cookies de tierce partie ?</li>
                    <p>
                      Les tiers peuvent utiliser des cookies pour diffuser et mesurer l'impact de leur contenu, dont les publicités, en lien avec vos intérêts sur Ressources Relationnelles et des sites tiers.
                      Les tierces parties peuvent également utiliser des cookies pour nous aider à acquérir de nouveaux utilisateurs et à mesurer l'efficacité de nos campagnes marketing.
                    </p>
                  </div>
                  <div>
                    <li>Comment contrôler les cookies ?</li>
                    <p>
                      Les utilisateurs de l'Espace économique européen (EEE) et du Royaume-Uni peuvent revoir leurs choix de cookies dans leurs Paramètres Sécurité et confidentialité Préférences de cookies. Il existe également des outils en ligne comme Browser Check, de la Digital Advertising Alliance, et le site Your Online Choices, de la European Digital Advertising Alliance qui expliquent la publicité numérique et vous permettent de choisir la façon dont les cookies sont utilisés sur internet en fonction de votre emplacement géographique, de l'appareil et du navigateur que vous utilisez. Tous les utilisateurs peuvent également activer l'extension de navigateur de Google pour désactiver Google Analytics sur tous les sites que vous visitez.
                      Vous pouvez également refuser d'accepter les cookies des services Ressources Relationnelles ou des développeurs d'extensions Ressources Relationnelles à tout moment en activant le paramètre de votre navigateur qui vous permet de refuser les cookies. Vous trouverez de plus amples informations sur la procédure à suivre pour désactiver les cookies sur le site internet du fournisseur de votre navigateur internet via l'écran d'aide. Pour en savoir plus, veuillez consulter la page Choix publicitaires [https://youradchoices.com/choices-faq] de la Digital Advertising Alliance. Veuillez noter que si les cookies sont désactivés, certaines fonctions des services Ressources Relationnelles ou des extensions peuvent ne pas fonctionner comme prévu.
                    </p>
                  </div>
                  <div>
                    <li>Comportements haineux et harcèlement</li>
                    <p>
                      Les comportements haineux et le harcèlement ne sont pas tolérés sur Ressources Relationnelles. Les comportement haineux désignent tout contenu ou toute activité qui promeut ou encourage la discrimination, le dénigrement, le harcèlement ou la violence sur la base des caractéristiques protégées suivantes : race, ethnie, couleur, caste, origine nationale, statut d’immigration, religion, sexe, genre, identité sexuelle, orientation sexuelle, handicap, état de santé grave et statut d’ancien combattant. Nous offrons également certaines protections pour l’âge. Ressources Relationnelles applique une tolérance zéro en ce qui concerne les comportements haineux, ce qui signifie que nous prenons des mesures pour chaque cas de comportement haineux valable signalé. En vertu de cette politique, nous offrons à tous les utilisateurs les mêmes protections, quelles que soient leurs caractéristiques individuelles.
                      Le harcèlement peut se manifester de nombreuses façons, comme le fait de suivre une personne à la trace, les attaques personnelles, l’apologie de violences physiques, les raids hostiles, et les faux signalements à plusieurs. Quant au harcèlement sexuel spécifiquement, il peut prendre la forme d’avances et de demandes sexuelles non souhaitées ou d’attaques dégradantes liées aux pratiques sexuelles perçues.
                      Nous prendrons des mesures pour tous les cas de comportements haineux et de harcèlement, avec une sévérité croissante lorsque le comportement est ciblé, personnel, explicite, répété ou prolongé, qu’il incite à d’autres exactions ou qu’il implique des menaces de violence ou de coercition. Les infractions les plus graves peuvent entraîner une suspension définitive dès le premier incident. 
                      En savoir plus sur nos politiques en matière de harcèlement et de comportements haineux et leur application.
                    </p>
                  </div>
                  <div>
                    <li>Partage non-autorisé d’informations privées</li>
                    <p>
                      Ne portez pas atteinte à la vie privée d’autrui. Il est interdit de partager du contenu pouvant révéler des informations personnelles privées sur des personnes ou leur propriété privée sans autorisation. Voici une liste non exhaustive des cas concernés :
                    </p>
                      <ul>
                        <li>Partage d’informations permettant d’identifier quelqu’un (comme son nom de famille officiel, le lieu où il vit ou ses papiers d'identité)</li>
                        <li>Partage de profils de réseaux sociaux restreints ou privés ou de toute information provenant de ces profils</li>
                        <li>Partage de contenus qui porte atteinte à l’attente raisonnable d’une autre personne en matière de vie privée, comme le fait de streamer depuis un espace privé sans autorisation</li>
                      </ul>
                  </div>
                  <div>
                    <li>Usurpation d’identité</li>
                    <p>
                      Tout contenu ou toute activité visant à usurper l’identité d’une personne ou entité est interdit. Toute tentative visant à vous faire passer pour un membre des représentants de Ressources Relationnelles constitue une violation qui sera traitée avec une tolérance zéro et entraînera une suspension pour une durée indéterminée.
                    </p>
                  </div>
                  <div>
                    <li>Nudité, pornographie et autre contenu à caractère sexuel</li>
                    <p>
                      La nudité et le contenu ou activités sexuellement explicites, tels que la pornographie, les actes ou rapports sexuels, et les services sexuels sont interdits.
                      Le contenu ou activités qui menacent ou incitent à la violence ou à l’exploitation sexuelle sont strictement interdits et peuvent être signalés aux forces de l’ordre. L’exploitation de mineurs sera signalée aux autorités via le National Center for Missing & Exploited Children.
                      Le contenu ou les activités à caractère sexuel sont également interdits, bien qu’ils puissent être autorisés dans des contextes éducatifs ou pour du contenu sous licence approuvé au préalable, dans chaque cas sous réserve de restrictions supplémentaires.
                    </p>
                  </div>
                  <div>
                    <li>Violence extrême, gore et autres comportements obscènes</li>
                    <p>
                      Tout contenu exclusivement centré sur des scènes sanglantes ou de violence gratuite ou extrême est interdit.
                    </p>
                  </div>
                </ol>
          </div>
      </section>
    );
  }
}