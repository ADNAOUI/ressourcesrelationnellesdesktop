import React from "react";
import { Step } from "./Wizard"

const Step6 = () => {

    const imageHandleClick = () => {
        var valeur = prompt("url de votre image");
        let img = document.getElementById("imageURL");

        img.src = valeur;
        document.getElementById("imageURLCard").setAttribute("src", valeur);
    }

    return (
        <div id="step6" className="col-12 content-wizard">
            <Step>
                <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>

                <article className="divided-content">
                    <div className="col-6">
                        <h2 className="title-left-content">Image de couverture</h2>
                        <div className="add-image-ressources">
                            <div className="add-image-ressources-icons" onClick={imageHandleClick}>
                                <i className="far fa-image"></i>
                                <i className="add-image-ressources-icons-plus fas fa-plus"></i>
                                <img id="imageURL" className="add-image-ressources-url" alt="" width="300px" height="300px" />
                            </div>
                        </div>
                        <p>
                            Le titre de la ressource est utilisé comme mot clé pour la fonction de recherche.
                        </p>
                    </div>
                </article>
            </Step>
        </div>
    )

}
export default Step6;