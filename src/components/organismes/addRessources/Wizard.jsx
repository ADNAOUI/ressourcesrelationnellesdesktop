import React from 'react'
import CardRessource from "./CardRessource";

import Logo from '../../atoms/img/logo/logoResponsive.png';

export const Wizard = ({ step: currentIndex, ...props }) => {
    const steps = React.Children.toArray(props.children);
    const stepsCurrent = steps[currentIndex].props.step + 1;
    const stepsLenght = steps.length;

    const prevStep = currentIndex !== 0 && steps[currentIndex - 1].props;
    const nextStep = currentIndex !== steps.length - 1 && steps[currentIndex + 1].props;

    return (
        <div className="display-wizard">
            <div className="steps-wizard">
                <span>
                    Étape {stepsCurrent}/{stepsLenght}
                </span>
            </div>
            <nav className="navigation-wizard">
                <img src={Logo} className="logo-wizard" alt="Logo Ressources Relationnelles" />
                {steps.map((step, index) => (
                    <button
                        key={step.props.title}
                        className={getClsNavBtn(index === currentIndex)}
                        title={step.props.description}
                    >
                        {step.props.title}
                    </button>
                ))}
            </nav>

            {steps[currentIndex]}

            {steps[currentIndex].props.title === "Introduction" ? "" : <CardRessource />}

            <div className="button-wizard-prev-next">
                <ButtonPrev
                    visible={prevStep}
                    onClick={() => props.onChange(currentIndex - 1)}
                    title={prevStep.description}
                >
                    Retour
                </ButtonPrev>
                <ButtonNext
                    visible={nextStep}
                    onClick={() => props.onChange(currentIndex + 1)}
                    title={nextStep.description}
                >
                    Suivant
                </ButtonNext>
            </div>
        </div>
    );
}

export const Step = ({ children }) => children;

function getClsNavBtn(active) {
    return "nav-btn-wizard flex-fill" + (active ? " active" : "");
}

function ButtonPrev({ visible, ...props }) {
    return (
        <button className={visible ? "btn-wizard btn-wizard-prev" : "invisible"} {...props} />
    );
}
function ButtonNext({ visible, ...props }) {
    return (
        <button className={visible ? "btn-wizard btn-wizard-next" : "invisible"} {...props} />
    );
}