import React from 'react';
import { NavLink } from "react-router-dom";
import ButtonLink from "../../atoms/links/ButtonLink.jsx";

//Images
import LogoMs_Lab from '../../atoms/img/logo/logoMs_Lab.png';
import Logo_Ministere from '../../atoms/img/logo/logoMinistere.png';
import Logo_Cesi from '../../atoms/img/logo/CESI.png';
import Logo from '../../atoms/img/logo/logoClair.png';


//Icones
import { AiOutlineInstagram, AiFillTwitterCircle } from "react-icons/ai";
import { IoLogoFacebook } from "react-icons/io";

//CSS
import './Footer.css'

const Footer = (props) => {

    const URLcourante = document.location.href.split('/');
    const pageActuelle = URLcourante[4];

    // On vérifie si le route existe ou non
    const activeRoute = (routeName) => {
        return props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    return (
        <footer className={pageActuelle === "presentation" ? "sectionFooterAjust" : "sectionFooter"}>
            <div className="parent-top-bottom-footer">

            <div className="parent-top-footer col-12">
                <div className="contact-footer col-4">
                    <div className="contact-footer-text">
                        <span className="bottom-footer-text upper bold font15 colorBlack">aucun paiement requis !</span>
                        <span className="bottom-footer-text ">Créer votre ressource dès aujourd'hui</span>
                    </div>
                    <ButtonLink class="button-contact-footer" name="Ajouter une ressource" url="/ressources_relationnelles/masterForm" />
                </div>
                <div className="logos-footer col-8">
                    <span className="bottom-footer-text">Nos Partenaires</span>
                    <div className="logos-footer-images">
                        <img src={Logo_Ministere} alt="Logo du ministère" />
                        <img src={LogoMs_Lab} alt="logo Ms_Lab" />
                        <img src={Logo_Cesi} alt="logo Ms_Lab" />
                    </div>
                </div>
            </div>

            <div className="parent-bottom-footer col-12">
                <div className="bottom-footer-logo col-3">
                    <img className="bottom-footer-logo-ressource-relationnelle" src={Logo} alt="Logo Ressources Relationnelles" />
                    <span className="bottom-footer-text"><small>&copy; 2021 Ressources Relationnelles Tout droit réservé</small></span>
                </div>

                <div className="bottom-footer-routes col-6">
                    <ul className="bottom-footer-routes-list">
                        {props.routesFooter.map((prop, key) => {
                            if (prop.redirect) return null;
                            return (
                                <li
                                    className={
                                        activeRoute(prop.layoutFooter + prop.pathFooter) +
                                        (prop.pro ? "active active-pro" : "")
                                    }
                                    key={key}
                                >
                                    <NavLink
                                        to={prop.layoutFooter + prop.pathFooter}
                                        className="nav-link"
                                        activeclass="active"
                                    >
                                        <label>{prop.nameFooter}</label>
                                    </NavLink>
                                </li>
                            );
                        })
                        }
                    </ul>
                </div>

                <div className="bottom-footer-social col-3">
                    <div className="bottom-footer-social-question">
                        <span className="bottom-footer-text bold">Des questions ?</span>
                        <span className="bottom-footer-text"><small><a className="bottom-footer-text underline" href="mailto:ms-lab@outlook.com">ms-lab@outlook.com</a></small></span>
                    </div>
                    <div className="bottom-footer-social-icons">
                        <AiOutlineInstagram />
                        <IoLogoFacebook />
                        <AiFillTwitterCircle />
                    </div>
                </div>
            </div>
            </div>

        </footer>
    );
}
export default Footer;