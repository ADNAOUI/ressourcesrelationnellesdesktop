//*----- MODULES -----//
import React from 'react';
import { Card } from 'react-bootstrap';

//icons
import heartIcon from '../../atoms/icons/heartIcon.png';
import commentIcon from '../../atoms/icons/commentIcon.png';
import shareIcon from '../../atoms/icons/shareIcon.png';
import starIcon from '../../atoms/icons/starIcon.png';

//*----- STYLES -----//
import './ResourceCard.css';

const ResourceCard = (props) => {

    return (
        <div id="resourceCard" className="resourceCard">
            <div id="lbl_publicationResourceCard" className="col-12 lbl_publicationResourceCard">
                <span id="span_publicationResourceCard" className="span_publicationResourceCard">publié le : <strong>{props.date}</strong></span>
            </div>

            <div className="cardRessource">
                <div className="cardBody">
                    <Card.Body>
                        <div className="row">
                            <div className="titleResourceCard col-12">
                                <Card.Title>
                                    <strong className="tailleTitreCardRessources">{props.title}</strong>
                                </Card.Title>
                            </div>
                        </div>
                        <div className="row">
                            <div className="contentPictureResourceCard col-12">
                                <Card.Img
                                    style={{ alignItems: 'center' }}
                                    src={props.image_ressource} alt=""
                                    width="290px"
                                    height="160px"
                                />
                            </div>
                        </div>
                        <div className="colorCardBadge">
                            <span className="badgeRessourceCard" variant="primary">{props.category}</span>
                            <span className="badgeRessourceCard" variant="secondary">{props.type_ressource}</span>
                            <span className="badgeRessourceCard" variant="secondary">{props.type_relation}</span>
                        </div>
                        <div className="row">
                            <div className="ProfilPictureResourceCard col-12">
                                <Card.Img
                                    style={{ width: '40px' }}
                                    width="40px"
                                    src={props.image_membre}
                                />
                                <span className="span_PseudonymeMembreResourceCard"><strong>{props.pseudo}</strong></span>
                                <span className="span_ProfessionMembreResourceCard">{props.job}</span>
                            </div>
                        </div>
                    </Card.Body>
                </div>
                <Card.Footer className="FooterResourceCard">
                    <div className="row">
                        <div className="iconResourceCard col-12">
                            <span><IconStatResourceCard name={heartIcon} number={props.nb_like} /></span>
                            <span><IconStatResourceCard name={commentIcon} number={props.nb_comments} /></span>
                            <span><IconStatResourceCard name={shareIcon} number={props.nb_share} /></span>
                            <span><IconStatResourceCard name={starIcon} /></span>
                        </div>
                    </div>
                </Card.Footer>
            </div>
        </div>
    );
}

function IconStatResourceCard(props) {
    return (
        <div>
            <Card.Img src={props.name} />
            <div className="ligneStat">
                <span className="span_numberIconResourceCard">{props.number}</span>
            </div>
        </div>
    )
}

export default ResourceCard;