//---- MODULES
import React from "react";

import './searchBar.css'

class SearchBar extends React.Component {

    handleInput(event) {
        const query = event.target.value.toLowerCase();
        const items = document.getElementsByClassName('resourceCard')
        requestAnimationFrame(() => {
            for (const item of items) {
                const shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
                shouldShow ? item.setAttribute("style", "display: block") : item.setAttribute("style", "display: none");
            }
        });
    }

    componentDidMount = () => {
        const searchbar = document.getElementById('search-bar')
        searchbar.addEventListener('input', this.handleInput)
    }

    render() {
        return (
            <div id="search-bar" className="col-5" >
                <input type="text" className="search__input" placeholder="Rechercher une ressource" />
                <i className="fas fa-search"></i>
            </div>
        );
    }
};

export default SearchBar