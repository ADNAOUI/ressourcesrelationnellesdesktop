//---- MODULES
import React from "react";
import { Link } from "react-router-dom";

const ButtonLink = (props) => {
    return(
        <Link to={props.url}>
            <button className={props.class}>{props.name}</button>
        </Link>
    )
}
export default ButtonLink;