//MODULES
import React, { Fragment, Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

//COMPONENTS
import ResourceCard from '../../../organismes/ressourceCard/ResourceCard';
import BackButtonMyAccount from '../../../atoms/img/monCompte/backButton.png';

//STYLES
import './encadrementPostResources.css';

class EncadrementPostResources extends Component {
   constructor(props){
      super(props);
      this.state = {
         ressources : [],
      }
   }
   getResources = () => {
      Axios({
      method:       'get',
      url:          `https://ms-lab-directus.fun/items/rr_ressources?limit=4`,
      responseType: 'json'
    }).then(response => {
        this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
    })
   }

   componentDidMount = () => {
      this.getResources();
   }

   render(){
      return(
         <Fragment>
            <div className="encadrementPostResources">
               <h1>{this.props.title}</h1>
               <div className="contentPostResources">
                  <ul className="display-ressources-list">
                     {this.state.ressources.map((ressource, i) =>
                        <Link key={i} to="/ressources_relationnelles/resourcecontainer">
                           <ResourceCard
                              date={ressource.DATE_PUBLICATION_RESSOURCE}
                              title={ressource.TITRE_RESSOURCE}
                              image_ressource={ressource.IMAGE_RESSOURCE}
                              category={ressource.DESIGNATION_CATEGORIE_RESSOURCE}
                              type_ressource={ressource.DESIGNATION_TYPE_RESSOURCE}
                              type_relation={ressource.DESIGNATION_TYPE_RELATION}
                              image_membre={ressource.IMAGE_PROFIL_MEMBRE}
                              pseudo={ressource.PSEUDO_MEMBRE}
                              job={ressource.PROFESSION_MEMBRE}
                              nb_like={ressource.NOMBRE_LIKE}
                              nb_comments={ressource.NOMBRE_COMMENTAIRE}
                              nb_share={ressource.NOMBRE_PARTAGE}
                           />
                        </Link>
                     )}
                  </ul>
                  <div className="btnEncadrement">
                     <Link to="/ressources_relationnelles/moncompte/route/ressources" ><img src={BackButtonMyAccount} alt="/"/></Link>
                  </div>
               </div>
            </div>
         </Fragment>
      )
   }
}
export default EncadrementPostResources;