//MODULES
import React, { Fragment, Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

//COMPONENTS
import ResourceCard from '../../../organismes/ressourceCard/ResourceCard';
import BackButtonMyAccount from '../../../atoms/img/monCompte/backButton.png';

//STYLES
import './encadrementGallery.css';

class EncadrementGallery extends Component {
   constructor(props){
      super(props);
      this.state = {
         ressources : []
      }
   }
   getResources = () => {
      Axios({
      method:       'get',
      url:          `https://ms-lab-directus.fun/items/rr_ressources?limit=15`,
      responseType: 'json'
    }).then(response => {
        this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
    })
   }
   componentDidMount = () => {
      this.getResources();
   }
   splitImgGallery = (png) => {
      console.log("png :", png);
      var pngString = png.toString();
      console.log("pngToString :", pngString);
      var pngSplit = pngString.split("?");
      console.log("pngToSplit :", pngSplit);
      return pngSplit[1];
   }
   render(){
      return(
         <Fragment>
            <div className="encadrementGallery">
               <h1>{this.props.title}</h1>
               <div className="contentPreviewFavoris">
                  <ul className="display-ressources-list">
                     {this.state.ressources.map((ressource, i) =>
                        <Link key={i} to={ressource.IMAGE_RESSOURCE}>
                           <div className="encadrementImgGallery">
                              <div>
                                 <img className="imgGallery" src={ressource.IMAGE_RESSOURCE} alt="/"/>
                                 <span className="spanGallery">{this.splitImgGallery(ressource.IMAGE_RESSOURCE)}.png</span>
                              </div>
                           </div>
                        </Link>
                     )}
                  </ul>
               </div>
            </div>
         </Fragment>
      )
   }

}
export default EncadrementGallery;