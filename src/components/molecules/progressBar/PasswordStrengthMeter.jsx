//MODULES
import React, { Fragment } from 'react';
import { passwordStrength } from 'check-password-strength';

//STYLES
import './passwordStrengthMeter.css';

const PasswordStrengthMeter = ({ motDePasseSignIn }) => {
     const scoresOptions = [{
               id          : 0,
               value       : "Très faible",
               minDiversity: 0,
               minLength   : 0
             },
             {
               id          : 1,
               value       : "Faible",
               minDiversity: 2,
               minLength   : 6
             },
             {
               id          : 2,
               value       : "Moyen",
               minDiversity: 3,
               minLength   : 10
             },
             {
               id          : 3,
               value       : "Fort",
               minDiversity: 4,
               minLength   : 20
             },
             {
               id          : 4,
               value       : "Excellent",
               minDiversity: 4,
               minLength   : 30
             }];
     const resultPassword = passwordStrength(motDePasseSignIn, scoresOptions);
     const securityScore = resultPassword.id * 100/4;

     const progressColor = () => {
          switch(resultPassword.id) {
               case 0 :
                    return '#828282';
               case 1 :
                    return '#EA1111';
               case 2 :
                    return '#FFAD00';
               case 3 :
                    return '#9BC158';
               case 4 :
                    return '#00B500';
               default:
                    return 'none';
          }
     }

     const changePasswordColor = () => ({
          width     : `${securityScore}%`,
          background: progressColor(),
          height    : '10px',
     })

     const passwordLabel = () => {
          switch(resultPassword.id) {
               case 0 :
                    return 'Très faible';
               case 1 :
                    return 'Faible';
               case 2 :
                    return 'Moyen';
               case 3 :
                    return 'Fort';
               case 4 :
                    return 'Excellent';
               default:
                    return '';
          }
     }

     return(
          <Fragment>
               <div className="password progress">
                    <div className="progress-bar" style={changePasswordColor()}></div>
               </div>
               <span className="passwordLabel" style={{color: progressColor()}}>{passwordLabel()}</span>

          </Fragment>
     )
}

export default PasswordStrengthMeter;