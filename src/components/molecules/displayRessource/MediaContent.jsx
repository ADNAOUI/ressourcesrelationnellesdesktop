import * as React from 'react';

const MediaContent = (props) => {

  return (
    <div className="media-content-ressource">
      <h1>{props.title}</h1>
      <div className="media-content-image" >
        <img src={props.image_ressource} alt={"cette image est le contenu de votre ressource qui s'intitule : " + props.title} />
      </div>
      <article>
        <p>
          {props.content}
        </p>
      </article>
    </div>

  );
}

export default MediaContent;