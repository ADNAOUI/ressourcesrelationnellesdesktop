//----- MODULES -----//
import React, { Component } from 'react'

//----- STYLES -----//

class ResourceIconBar extends Component {
    render() {
        return (
            <div className="social-content-icons">
                <span>Partage</span>
                <ul className="social-content-icons-list">
                    <li>
                        <i className="fab fa-facebook"></i>
                    </li>
                    <li>
                        <i className="fas fa-envelope"></i>
                    </li>
                    <li>
                        <i className="fab fa-instagram-square"></i>
                    </li>
                    <li>
                        <i className="fab fa-discord"></i>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ResourceIconBar;