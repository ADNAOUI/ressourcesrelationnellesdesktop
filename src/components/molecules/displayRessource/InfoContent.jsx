//----- MODULES -----//
import * as React from 'react'

import './displayRessource.css'

const InfoContent = (props) => {
    return (
        <div className="perimetreOverlayInfo">
            <div className="info-content-user">
                <img
                    style={{ width: '40px' }}
                    width="40px"
                    src={props.image_membre}
                    alt=" "
                />
                <span className="info-content-user-name-job">
                    <strong>{props.pseudo}</strong>
                    <small>{props.job}</small>
                </span>
            </div>

            <div className="overlayInfo Rubrique">
                <div className="colorCardBadge">
                    <span className="badgeRessourceCard" variant="primary">{props.category}</span>
                    <span className="badgeRessourceCard" variant="secondary">{props.type_ressource}</span>
                    <span className="badgeRessourceCard" variant="secondary">{props.type_relation}</span>
                </div>
            </div>

            <hr className="info-content-separation" />

            <div className="overlayInfo Informations">
                <span id="span_publicationResourceCard" className="span_publicationResourceCard">
                    <strong>{props.date}</strong>
                </span>
                <p className="display-ressource-small-content">
                    {props.content}
                </p>
            </div>

        </div>
    );
}

export default InfoContent;