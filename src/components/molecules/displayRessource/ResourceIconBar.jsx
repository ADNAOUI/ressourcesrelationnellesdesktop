//----- MODULES -----//
import React, { Component } from 'react'

//----- STYLES -----//

const ResourceIconBar = (props) => {
    return (
        <>
            <ul className="display-ressource-icons">
                <li>
                    <i className="far fa-heart">  </i>
                    <span> {props.nbr_heart} </span>
                </li>
                <li>
                    <i className="far fa-comment-alt"></i>
                    <span>{props.nbr_comments}</span>
                </li>
                <li>
                    <i className="fas fa-share-alt"></i>
                    <span>{props.nbr_share}</span>
                </li>
            </ul>

        </>
    );
}

export default ResourceIconBar;