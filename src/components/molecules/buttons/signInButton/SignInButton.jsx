//----- MODULES
import React, { Component, useState} from "react";
import Axios from 'axios';
import { Redirect } from "react-router-dom";
import { Form, Modal } from "react-bootstrap";
import { hashSync } from 'bcryptjs';

//COMPONENTS
import PasswordStrengthMeter from '../../progressBar/PasswordStrengthMeter';

//----- MULTIMEDIAS
import LogoModal from "../../../atoms/img/logo/logoResponsive.png";
import LookPasswordIcon from '../../../atoms/icons/lookPassword.png';

//STYLES
import './signInButton.css';
import context from "react-bootstrap/esm/AccordionContext";

class SignIn extends Component {
    //*----- INSTANCIATION -----*//
    constructor(props){
        super(props);
        this.state={
            //*----- STATES BdD -----*//
            membres:           [],

            //*----- STATES LOCALES -----*//
            pseudonymeLogIn:   "",
            pseudonymeSignIn:  "",
            nom:               "",
            prenom:            "",
            dateDeNaissance:   "",
            eMail:             "",
            motDePasseLogIn:   "",
            motDePasseSignIn:  "",
            confirmMotDePasse: "",
        }
    }
    render() {
        return(
            <div>
                <SignInButton datas={this.state}/>
            </div>
        )
    }
}

function SignInButton(props){
    /*VARIABLES*/
    const [indexTabs , setIndexTabs]                = useState(1);
    const [pseudonymeSignIn, setPseudonymeSignIn]   = useState('');
    const [pseudonymeLogIn, setPseudonymeLogIn]     = useState('');
    const [nom, setNom]                             = useState('');
    const [prenom, setPrenom]                       = useState('');
    const [dateDeNaissance, setDateDeNaissance]     = useState('');
    const [eMail, setEmail]                         = useState('');
    const [motDePasseSignIn, setMotDePasseSignIn]   = useState(null);
    const [motDePasseLogIn, setMotDePasseLogIn]     = useState(null);
    const [confirmMotDePasse, setConfirmMotDePasse] = useState(null);
    const [membres, setMembres]                     = useState([]);
    const [salt, setSalt]                           = useState(10);

    /*FONCTIONS*/
    const indexTabsContent = (index) => {
        setIndexTabs(index);
    }
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleSubmitLogIn = (event) => {
        event.preventDefault();
        const getMembres = Axios({
            method:       'get',
            url:          `https://ms-lab-directus.fun/items/membres`,
            responseType: 'json',
            }).then(response => {
            setMembres([...response.data.data]);
            });
            handleClose();
    }

    const handleSubmitSignIn = (event) => {
        event.preventDefault();
        Axios({
            method:       'post',
            url:          `https://ms-lab-directus.fun/./items/rr_membres`,
            data: {
                NOM_MEMBRE:             nom,
                PRENOM_MEMBRE:          prenom,
                NOM_UTILISATEUR_MEMBRE: pseudonymeSignIn,
                PASSWORD_MEMBRE:        hashSync(motDePasseSignIn, salt),
                DATE_NAISSANCE:         dateDeNaissance,
                MAIL_MEMBRE:            eMail
            }
        }).then((response => {
            alert('Data envoyés');
        }
        )).catch(function(erreur){
        });
    }

    const handleClick = (event) => {
        const inputSignIn = document.getElementsByClassName('inputSignIn');
        const verificateConfirmPassword = (motDePasseSignIn) => {
            if (motDePasseSignIn !== confirmMotDePasse & motDePasseSignIn !== null & confirmMotDePasse !== ""){
                alert('Je suis dans le vérificateur de confiration de mot de passe');
                return alert('Envoyé');
            }
        }
        for (let index = 0; index < inputSignIn.length; index++){
            if(inputSignIn[index].value === ""){
                inputSignIn[index].classList.add("rouge");
                inputSignIn[index].style.setProperty('--c', "white");
                document.getElementById("errorMsg").textContent = "Des erreurs ont été détectées sur les champs en surbruillance rouge !";
                verificateConfirmPassword(motDePasseSignIn);
            }else if(inputSignIn[index].value !== ""){
                inputSignIn[index].classList.remove("rouge");
            }
        }
    }

    const handlePointerEnter = () => {
        const passwordHelpBlock = document.getElementById("passwordHelpBlock");
        passwordHelpBlock.style.opacity = "1";
    }
    const handlePointerLeave = () => {
        const passwordHelpBlock = document.getElementById("passwordHelpBlock");
        passwordHelpBlock.style.opacity = "0";
    }

    const handleClickLookPassword = () => {
        const Password = document.getElementById('motDePasseSignIn');
        if(Password.type === "password") {
            return Password.type = "text";
        }else if(Password.type === "text"){
            return Password.type = "password";
        }
    }
    const handleClickLookConfirmPassword = () => {
        const ConfirmPassword = document.getElementById('confirmMotDePasse');
        if(ConfirmPassword.type === "password") {
            return ConfirmPassword.type = "text";
        }else if(ConfirmPassword.type === "text"){
            return ConfirmPassword.type = "password";
        }
    }
    const handleVerificateConfirmPassword = (event) => {
        const ConfirmPassword = document.getElementById('confirmMotDePasse');
        if(motDePasseSignIn === confirmMotDePasse){
            ConfirmPassword.classList.add("validateInput");
        }else{
            ConfirmPassword.classList.remove("validateInput");
        }
    }

    const handleChangePseudonymeSignIn = (event) => {
        setPseudonymeSignIn(event.target.value);
    }
    const handleChangePseudonymeLogIn = (event) => {
        setPseudonymeLogIn(event.target.value);
    }
    const handleChangeNom = (event) => {
        setNom(event.target.value);
    }
    const handleChangePrenom = (event) => {
        setPrenom(event.target.value);
    }
    const handleChangeDateDeNaissance = (event) => {
        setDateDeNaissance(event.target.value);
    }
    const handleChangeEmail = (event) => {
        setEmail(event.target.value);
    }
    const handleChangeMotDePasseSignIn = (event) => {
        setMotDePasseSignIn(event.target.value);
    }
    const handleChangeMotDePasseLogIn = (event) => {
        setMotDePasseLogIn(event.target.value);
    }
    const handleChangeConfirmMotDePasse = (event) => {
        setConfirmMotDePasse(event.target.value);
    }

    return (
        <div className="parentButtonPositionInscriptionTopNavBar">
            <button className="buttonInscription" onClick={handleShow}>S'inscrire</button>
            <Modal className="formSignIn" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <img src={LogoModal} alt="Logo ressource relationelles" className="logoModal" />
                    <Modal.Title id="inscription" className={indexTabs === 1 ? 'activeContent centerTitle' : 'content'}>S'inscrire à <br/>Ressources Relationnelles</Modal.Title>
                    <Modal.Title id="connexion" className={indexTabs === 2 ? 'activeContent  centerTitle' : 'content'}>Se connecter à <br/>Ressources Relationnelles</Modal.Title>
                    <div className="positionBoutonInscriptionConnexion" >
                        <a href="#inscription" className={indexTabs === 1 ? 'activeText col-6' : 'col-6 btnTopNavBarInsCon'} onClick={() =>  indexTabsContent(1) }>S'inscrire </a>
                        <a href="#connexion" className={indexTabs === 2 ? 'activeText col-6' : 'col-6 btnTopNavBarInsCon'} onClick={() =>  indexTabsContent(2) }>Se connecter </a>
                    </div>
                </Modal.Header>

                <Modal.Body className={indexTabs === 1 ? 'activeContent' : 'content'}>
                    <Form onSubmit={handleSubmitSignIn}>
                        <Form.Group controlId="formInscriptionPseudo">
                            <Form.Label>
                                Pseudonyme
                            </Form.Label>
                            <Form.Control id="pseudonymeSignIn" className="inputSignIn" type="text" placeholder="Entrez un pseudonyme" onChange={handleChangePseudonymeSignIn} />
                        </Form.Group>
                        <Form.Group  controlId="formInscriptionName">
                            <Form.Label >
                                Nom
                            </Form.Label>
                            <Form.Control id="nom" className="inputSignIn" type="text" placeholder="Entrez votre nom" onChange={handleChangeNom} />
                        </Form.Group>
                        <Form.Group  controlId="formInscriptionPrenom">
                            <Form.Label >
                                Prénom
                            </Form.Label>
                            <Form.Control id="prenom" className="inputSignIn" type="text" placeholder="Entrez votre prénom" onChange={handleChangePrenom} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Date de naissance</Form.Label>
                            <Form.Control id="dateDeNaissance" className="inputSignIn" type="date" onChange={handleChangeDateDeNaissance} />
                        </Form.Group>
                        <Form.Group controlId="formInscriptionEmail">
                            <Form.Label >
                                E-mail
                            </Form.Label>
                            <Form.Control id="eMail" className="inputSignIn" type="email" placeholder="Entrez une adresse mail valide" onChange={handleChangeEmail} />
                        </Form.Group>
                        <Form.Text id="passwordHelpBlock" muted>
                            Votre <strong>mot de passe</strong> doit comporter entre 8 et 20 caractères, contenir des lettres et des chiffres, et ne doit pas contenir d'espaces, de caractères spéciaux ou d'emoji.
                        </Form.Text>
                        <Form.Group controlId="formInscriptionPassword" className="formInscriptionPassword">
                            <Form.Label >
                                Mot de passe
                            </Form.Label>
                            <Form.Control id="motDePasseSignIn" className="inputSignIn" type="password" placeholder="Choissisez un mot de passe" onChange={handleChangeMotDePasseSignIn} onPointerEnter={handlePointerEnter} onPointerLeave={handlePointerLeave} onKeyUp={handleVerificateConfirmPassword} />
                            <img className="lookPasswordIcon2" src={LookPasswordIcon} alt="Afficher la saisie du mot de passe" onClick={handleClickLookPassword}/>
                            <PasswordStrengthMeter motDePasseSignIn={motDePasseSignIn} />
                        </Form.Group>
                        <Form.Group  controlId="formInscriptionPasswordConfirm">
                            <Form.Label >
                                Confirmez votre mot de passe
                            </Form.Label>
                            <Form.Control id="confirmMotDePasse" className="inputSignIn" type="password" placeholder="Confirmer un mot de passe" onChange={handleChangeConfirmMotDePasse} onKeyUp={handleVerificateConfirmPassword} />
                            <img className="lookPasswordIcon" src={LookPasswordIcon} alt="Afficher la saisie du mot de passe" onClick={handleClickLookConfirmPassword}/>
                        </Form.Group>
                        <p className="text-privacy-conditions">
                            En cliquant sur s'inscrire, vous reconnaissez avoir lu et approuvé les
                            <a href="/ressources_relationnelles/conditions">Conditions d'utilisations</a>
                            et la
                            <a href="/ressources_relationnelles/politicalCookies">Politique de confidentialité.</a>
                        </p>
                        <Form.Text id="errorMsg"></Form.Text>
                        <input type="submit" className={indexTabs === 1 ? 'activeContent buttonInscription' : 'content buttonInscription'} onClick={handleClick} value="S'inscrire"/>
                        <input type="submit" className={indexTabs === 2 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="Se connecter"/>
                    </Form>
                </Modal.Body>

                <Modal.Body className={indexTabs === 2 ? 'activeContent' : 'content'}>
                    <Form className="fullWidthForm" onSubmit={handleSubmitLogIn}>
                        <Form.Group  controlId="formConnexionID">
                            <Form.Label >
                                Identifiant
                            </Form.Label>
                            <Form.Control type="text" placeholder="Entrez votre identifiant" onChange={handleChangePseudonymeLogIn} />
                        </Form.Group>

                        <Form.Group  controlId="formConnexionPassword">
                            <Form.Label >
                                Mot de passe
                            </Form.Label>
                            <Form.Control type="password" placeholder="Entrez votre mot de passe" onChange={handleChangeMotDePasseLogIn} />
                            <Form.Text>Mot de passe oublié ?</Form.Text>
                        </Form.Group>
                        <input type="submit" className={indexTabs === 1 ? 'activeContent buttonInscription' : 'content buttonInscription'} onClick={handleClick} value="S'inscrire"/>
                        <input type="submit" className={indexTabs === 2 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="Se connecter"/>
                    </Form>
                </Modal.Body>
            </Modal>
        </div>
    )
}
export default SignIn;